# 好睡眠

#### 主页
[网站介绍](https://nasio-limbo.github.io/asleepings.html)

#### 介绍
每一个热爱生活的人，都值得拥有好睡眠，好睡眠就用好睡眠。
好睡眠提供白噪音、助眠曲等功能。
专业、全面和暖心的睡眠服务，帮您睡得更好睡得更香，更希望籍此唤起您对睡眠重要性和睡眠质量的关注。
在睡眠管理基础上，好睡眠白噪音是您提高工作、学习、生活效率好帮手，也是您冥想静心、灵感碰撞、看书阅读的小伴侣。

#### 安装
[酷安](https://www.coolapk.com/apk/287232)

[华为商店](https://appgallery.huawei.com/#/app/C104643881)

#### 预览
<div align="center">
    <img src="screenshots/1.png" width="375" height="666"/><br/>    
    <img src="screenshots/2.png" width="375" height="666"/><br/>
    <img src="screenshots/3.png" width="375" height="666"/><br/>
    <img src="screenshots/4.png" width="375" height="666"/><br/>
</div>
